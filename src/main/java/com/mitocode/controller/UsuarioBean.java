package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;
	private Usuario usuario;
	private List<Usuario> lista;
	private String tipoDialog;

	private String nuevacontrasena;
	private String repertircontrasena;
	private Boolean habilitarBotonAceptar;
	private String cadenaBusqueda;

	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.lista = new ArrayList<>();
		this.listar();
		this.habilitarBotonAceptar = true;
	}

	public void listar() {
		try {
			this.lista = this.usuarioService.listar();
		} catch (Exception e) {

		}
	}

	public void operar() {
		try {
			this.usuarioService.modificar(this.usuario);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			this.limpiarControles();
		}
	}

	public void mostrarData(Usuario u) {
		this.usuario = u;
		this.tipoDialog = "Modificar usuario: " + u.getUsuario();
	}

	public void limpiarControles() {
		this.usuario = new Usuario();
		this.tipoDialog = "Nuevo";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getTipoDialog() {
		return tipoDialog;
	}

	public void setTipoDialog(String tipoDialog) {
		this.tipoDialog = tipoDialog;
	}

	public String getNuevacontrasena() {
		return nuevacontrasena;
	}

	public void setNuevacontrasena(String nuevacontrasena) {
		this.nuevacontrasena = nuevacontrasena;
	}

	public String getRepertircontrasena() {
		return repertircontrasena;
	}

	public void setRepertircontrasena(String repertircontrasena) {
		this.repertircontrasena = repertircontrasena;
	}

	public Boolean getHabilitarBotonAceptar() {
		return habilitarBotonAceptar;
	}

	public void setHabilitarBotonAceptar(Boolean habilitarBotonAceptar) {
		this.habilitarBotonAceptar = habilitarBotonAceptar;
	}
	
	public String getCadenaBusqueda() {
		return cadenaBusqueda;
	}

	public void setCadenaBusqueda(String cadenaBusqueda) {
		this.cadenaBusqueda = cadenaBusqueda;
	}
	

	public void VerificarPwd() {
		this.habilitarBotonAceptar = true;
		try {
			Usuario us = this.usuarioService.login(this.usuario);

			if (us != null && us.getEstado().equalsIgnoreCase("A")) {
				this.habilitarBotonAceptar = false;
			}
			else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Password Incorrecto..!"));			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Password Incorrecto..!"));			
			e.printStackTrace();
		}
	}
	public void cambiarPwd() {
		try {
			if (this.nuevacontrasena == null || this.nuevacontrasena.trim().isEmpty()) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", 
						"La nueva contrase�a No puede estar vac�a..!"));
				return;
			}
			String clave = this.nuevacontrasena;
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setContrasena(claveHash);
			int res = this.usuarioService.modificar(this.usuario);
			if (res > 0) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success!",
						"Cambio de Password Correcto..!"));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", 
						"Error al cambiar Password..!"));
			}
		} catch (Exception e) {
						
			e.printStackTrace();
		}
	}
	public void buscarUsuarios() {
		try {
			if (this.cadenaBusqueda == null || this.cadenaBusqueda.trim().isEmpty()) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", 
						"La cadena de b�squeda No puede estar vac�a..!"));
				return;
			}
			this.lista = this.usuarioService.buscarUsuarios(getCadenaBusqueda());
		} catch (Exception e) {
						
			e.printStackTrace();
		}
	}
}